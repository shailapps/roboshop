/*
 * Shared pipeline for cloud native services
 */
def getRundeckParams(String params, String optionName, String optionValue) {
  def rundeckParams = ''
  if (optionName) {
    print optionName + ' ' + optionValue
    rundeckParams = ' -' + optionName + ' ' + optionValue
  }
  if(params) {
    print params
    def paramsList = params.split(',')
    paramsList.each {
      def paramItem = it.split('=')
      rundeckParams = rundeckParams + ' -' + paramItem[0].trim() + " '" + paramItem[1].trim() + "'"
    }
  }

  return rundeckParams
}

def call(Map params) {

  pipeline {
    agent { label 'linux' }

    triggers {
      cron(env.BRANCH_NAME == "main" || env.BRANCH_NAME == "master" || env.BRANCH_NAME ==~ /release.*/ ? 'H H H(1-7) * *': '')
    }

    options {
      buildDiscarder(logRotator(daysToKeepStr: '30', numToKeepStr: '20'))
      timeout(time: 1, unit: 'HOURS')
    }

    tools {
      maven 'm3'
      nodejs 'nodejs'
    }

    environment {
      MAVEN_OPTS = ' -Djavax.net.ssl.trustStore=/newarch/apps/jenkins/jenkins.truststore.jks'
      branchName = "${env.BRANCH_NAME}"
      PATH = "${PATH}:/newarch/apps/graal/bin"
      GRAALVM_HOME = "/newarch/apps/graal"
      jdkEnv = 'graalvm'
      versionQualifier = "${params.versionQualifier}"
    }

    stages {
      stage('Build and verify') {
        steps {
          script {
            sh "env"
            failedStage=env.STAGE_NAME
            if(params.jdkVersion) {
              jdkEnv = params.jdkVersion
            }
            if(params.branch) {
              branchName = params.branch
            }
            deployToDEMO = true
            if(params.deployToDEMO !=null &&Boolean.toString(params.deployToDEMO)=="false") {
              deployToDEMO = false
            } 
            deployToQA=params.deployToQA
            image = null

            gfsNotifyGit(commitSha1: env.GIT_COMMIT)
          }
          withEnv(["JAVA_HOME=${tool jdkEnv}", "PATH=${tool jdkEnv}/bin:${env.PATH}"]) {
            sh "java -version"
            sh "echo m2 home=${M2_HOME}"
            sh "echo path=${PATH}"
            sh "echo commit id=${env.GIT_COMMIT}"
            script {
              rundeckQAConfig = gfsPOMPropertyValue('qa.rundeck.job.params', null, jdkEnv)
              if(!rundeckQAConfig) {
                deployToQA = false
              }
            }
            sh "mvn clean verify"
            script {
              sonarProj = gfsPOMPropertyValue('sonar.projectName', null, jdkEnv)
              if(sonarProj != null && (branchName == "main" || branchName == "master" || branchName ==~ /release.*/)) {
                gfsJavaSonarqubeScan()
              }
            }
          }
        }
      }
      stage('Scan') {
        when {
          expression { branchName == "main" || branchName == "master" || branchName ==~ /hotfix.*/ || branchName ==~ /release.*/ }
        }
        steps {
          withEnv(["JAVA_HOME=${tool jdkEnv}", "PATH=${tool jdkEnv}/bin:${env.PATH}"]) {
            script {
              failedStage = env.STAGE_NAME
              dirsToScan = sh(script: 'ls -d */ | grep -vE ".-wc/$|.-container/$|newman|target/|regression/$|.-ear/$"', returnStdout: true)
              scaAppName = gfsPOMPropertyValue('fortify.sscApplicationName', null, jdkEnv)
              scaAppVersion = gfsPOMPropertyValue('fortify.sscApplicationVersion', null, jdkEnv)
              sonatypeAppName = gfsPOMPropertyValue('sonatype.appId', scaAppName + '_' + scaAppVersion, jdkEnv)
            }
            catchError(buildResult: 'UNSTABLE', stageResult: 'UNSTABLE') {
              gfsJavaFortifyScan(jdkEnv: jdkEnv, scaAppName: scaAppName, scaAppVersion: scaAppVersion, useFeadFortifyAuth: params.useFeadFortifyAuth )

              withCredentials([usernamePassword(credentialsId: 'sonatype', usernameVariable: 'user', passwordVariable: 'pass')]) {
                sh "/newarch/apps/sonatype/nexus_iq_cli.sh ${sonatypeAppName} ${user}:${pass} '''${dirsToScan}'''"
              }
            }
          }
        }
      }
      stage('Release') {
        when {
          allOf {
            not {
              triggeredBy 'TimerTrigger'
            }
            expression {
              branchName == "main" || branchName == "master" || branchName ==~ /hotfix.*/ || branchName ==~ /release.*/
            }
          }
        }
        steps {
          script {
            releaseOption = params.releaseOptions
            failedStage = env.STAGE_NAME
            withEnv(["JAVA_HOME=${tool jdkEnv}", "PATH=${tool jdkEnv}/bin:${env.PATH}"]) {
              if(params.versionQualifier) {
                if(branchName ==~ /hotfix.*/) {
                  sh 'mvn build-helper:parse-version versions:set -DnewVersion=\'${parsedVersion.majorVersion}.${parsedVersion.minorVersion}.\'${parsedVersion.incrementalVersion}-' + params.versionQualifier + BUILD_NUMBER
                } else {
                  sh 'mvn build-helper:parse-version versions:set -DnewVersion=\'${parsedVersion.majorVersion}.${parsedVersion.minorVersion}.\'${BUILD_NUMBER}-' + params.versionQualifier
                }
              } else {
                if(branchName ==~ /hotfix.*/) {
                  sh 'mvn build-helper:parse-version versions:set -DnewVersion=\'${parsedVersion.majorVersion}.${parsedVersion.minorVersion}.${parsedVersion.incrementalVersion}-\'${BUILD_NUMBER}'
                } else {
                  sh 'mvn build-helper:parse-version versions:set -DnewVersion=\'${parsedVersion.majorVersion}.${parsedVersion.minorVersion}.\'${BUILD_NUMBER}'
                }
              }

              if (params.serviceType == "quarkus") {
                sh "${GRAALVM_HOME}/bin/gu install native-image"
              }
             // sh "mvn clean deploy ${releaseOption} scm:tag -DMessage=\"NSS-104: automatic release\" -DskipTests=true"
              artifactId = gfsPOMPropertyValue('project.artifactId', null, jdkEnv)
              version = gfsPOMPropertyValue('project.version', null, jdkEnv)
              sh "mvn clean deploy ${releaseOption} -DskipTests=true"
              sh "git tag ${artifactId}-${version}"

              if(env.GIT_URL.startsWith('https://')) {
                withCredentials([usernamePassword(credentialsId: 'svc-newarch-cicd-gitlab', usernameVariable: 'user', passwordVariable: 'pass')]) {
                  urlWithCredential = env.GIT_URL
                  urlWithCredential = urlWithCredential.replace("gitlab", user + ':' + pass + '@gitlab')
                  sh "git remote set-url origin ${urlWithCredential}"
                }
                sh "git push --tags >/dev/null 2>&1"
              }
              else {
                sh "git push --tags"
              }
            }
          }
        }
      }


      stage('Build container image') {
        when {
          allOf {
            not {
              triggeredBy 'TimerTrigger'
            }
            expression {
              params.buildContainerImage == true &&
                (branchName == "main" || branchName == "master" || branchName ==~ /hotfix.*/ || branchName ==~ /release.*/)
            }
          }
        }
        steps {
          withEnv(["JAVA_HOME=${tool jdkEnv}", "PATH=${tool jdkEnv}/bin:${env.PATH}"]) {
            script {
              failedStage = env.STAGE_NAME
              artifactId = gfsPOMPropertyValue('project.artifactId', null, jdkEnv)
              groupId = gfsPOMPropertyValue('project.groupId', null, jdkEnv)
              version = gfsPOMPropertyValue('project.version', null, jdkEnv)
              uaid = gfsPOMPropertyValue('fs.app.uaid', scaAppName, jdkEnv)
              imageNexus ="fmk.nexus-ci.onefiserv.net/org/is"

              image = gfsContainerImageBuilder(
                  artifactId: artifactId,
                  version: version,
                  uaid: uaid,
                  type: params.serviceType,
                  imageNexus: imageNexus)
            }
          }
        }
      }
      stage('Scan container image') {
        when {
          allOf {
            not {
              triggeredBy 'TimerTrigger'
            }
            expression {
              params.buildContainerImage == true &&
                (branchName == "main" || branchName == "master" || branchName ==~ /hotfix.*/ || branchName ==~ /release.*/)
            }
          }
        }
        steps {
          withEnv(["JAVA_HOME=${tool jdkEnv}", "PATH=${tool jdkEnv}/bin:${env.PATH}"]) {
            script {
              scanPolicy = gfsPOMPropertyValue('image.scan.policy', 'issuer-solution-ci-scan-policy', jdkEnv)
              scanTag = "application=" + scaAppName
            }
          }
          catchError(buildResult: 'UNSTABLE', stageResult: 'UNSTABLE') {
            gfsContainerImageScan(imageName: image.imageName, scanPolicy: scanPolicy, scanTag: scanTag)
          }
        }
      }
      stage('Release container image') {
        when {
          allOf {
            not {
              triggeredBy 'TimerTrigger'
            }
            expression {
              params.buildContainerImage == true &&
                (branchName == "main" || branchName == "master" || branchName ==~ /hotfix.*/ || branchName ==~ /release.*/)
            }
          }
        }
        steps {
          gfsContainerImageNexusPush(imageName: image.imageName)
        }
      }
      stage('Deploy to DEMO') {
          when {
              allOf {
                  not {
                      triggeredBy 'TimerTrigger'
                  }
                  expression {
                      deployToDEMO && (branchName == "main" || branchName == "master" || branchName ==~ /hotfix.*/ || branchName ==~ /release.*/)
                  }
              }
          }
          steps {
              withEnv(["JAVA_HOME=${tool jdkEnv}", "PATH=${tool jdkEnv}/bin:${env.PATH}"]) {
                  script {
                      failedStage = env.STAGE_NAME
                      
                      // Determine whether to use Harness or Rundeck
                      def useHarness = sh(script: 'mvn help:evaluate -Dexpression=use.harness -q -DforceStdout', returnStdout: true).toBoolean()
                      
                      if (useHarness) {
                          // Harness deployment
                          def harnessParams = [
                              accountId: sh(script: 'mvn help:evaluate -Dexpression=harness.account.id -q -DforceStdout', returnStdout: true).trim(),
                              orgIdentifier: sh(script: 'mvn help:evaluate -Dexpression=harness.org.identifier -q -DforceStdout', returnStdout: true).trim(),
                              projectIdentifier: sh(script: 'mvn help:evaluate -Dexpression=harness.project.identifier -q -DforceStdout', returnStdout: true).trim(),
                              pipelineIdentifier: sh(script: 'mvn help:evaluate -Dexpression=harness.pipeline.identifier -q -DforceStdout', returnStdout: true).trim(),
                              triggerIdentifier: sh(script: 'mvn help:evaluate -Dexpression=harness.trigger.identifier -q -DforceStdout', returnStdout: true).trim(),
                              version: sh(script: 'mvn help:evaluate -Dexpression=project.version -q -DforceStdout', returnStdout: true).trim(),
                              branch: env.GIT_BRANCH
                          ]
                          
                          gfsHarnessPipeline(harnessParams)
                      } else {
                          // Existing Rundeck deployment (unchanged)
                          rundeckUrl = gfsPOMPropertyValue('rundeck.url', null, jdkEnv)
                          rundeckToken = gfsPOMPropertyValue('rundeck.token', null, jdkEnv)
                          if(rundeckToken.startsWith('null object')) {
                              rundeckToken = sh script: "/newarch/apps/jenkins/rundeck_token.sh ${rundeckUrl}", returnStdout: true
                          }
                          rundeckJob = sh script: 'mvn help:evaluate -Dexpression=rundeck.job -q -DforceStdout', returnStdout: true
                          rundeckOptionName = sh script: 'mvn help:evaluate -Dexpression=rundeck.job.option.name -q -DforceStdout', returnStdout: true
                          if(rundeckOptionName.startsWith('null object')) {
                              rundeckOptionName = null}
                          rundeckOptionValue = sh script: 'mvn help:evaluate -Dexpression=rundeck.job.option.value -q -DforceStdout', returnStdout: true
                          if(rundeckOptionValue.startsWith('null object'))
                          {
                              rundeckOptionValue = null
                          }
                          rundeckJobParams = sh script: 'mvn help:evaluate -Dexpression=rundeck.job.params -q -DforceStdout', returnStdout: true
                          rundeckOptions = getRundeckParams(rundeckJobParams, rundeckOptionName, rundeckOptionValue)
                          print 'rundeck params for dev:' + rundeckOptions

                          gfsRundeckJob(version: '',
                                  param: rundeckOptions,
                                  token: rundeckToken,
                                  host: rundeckUrl,
                                  job: rundeckJob)
                      }
                  }
              }
          }
      }
      stage('DEMO regression test') {
        when {
          allOf {
            not {
              triggeredBy 'TimerTrigger'
            }
            expression {
              deployToDEMO && (branchName == "main" || branchName == "master" || branchName ==~ /hotfix.*/ || branchName ==~ /release.*/)
            }
          }
        }
        steps {
          script {
            failedStage=env.STAGE_NAME
            waitTime=params.waitTimeForRegressions
            if(waitTime) {
              echo "Waiting ${waitTime} seconds for start up to complete prior starting regressions"
              sleep waitTime.toInteger()
            }
          }
          sh "echo trigger dev regression job for DEMO"
          sh "sh ./regression.sh DEMO"
        }
      }
      stage('Deploy to QA') {
        when {
          allOf {
            not {
              triggeredBy 'TimerTrigger'
            }
            expression {
              deployToQA && (branchName == "main" || branchName == "master" || branchName ==~ /release.*/)
            }
          }
        }
        steps {
          script {
            failedStage = env.STAGE_NAME
          }
          sh "echo deploy to qa"
          withEnv(["JAVA_HOME=${tool jdkEnv}", "PATH=${tool jdkEnv}/bin:${env.PATH}"]) {
            script {
              rundeckUrl = gfsPOMPropertyValue('rundeck.url', null, jdkEnv)
              rundeckToken = gfsPOMPropertyValue('rundeck.token', null, jdkEnv)
              rundeckJob = sh script: 'mvn help:evaluate -Dexpression=rundeck.job -q -DforceStdout', returnStdout: true

              if(rundeckToken.startsWith('null object')) {
                rundeckToken = sh script: "/newarch/apps/jenkins/rundeck_token.sh ${rundeckUrl}", returnStdout: true
              }
              rundeckJobParams = sh script: 'mvn help:evaluate -Dexpression=qa.rundeck.job.params -q -DforceStdout', returnStdout: true
              rundeckOptionName = sh script: 'mvn help:evaluate -Dexpression=rundeck.job.option.name -q -DforceStdout', returnStdout: true
              if(rundeckOptionName.startsWith('null object')) {
                rundeckOptionName = null
              }
              rundeckOptionValue = sh script: 'mvn help:evaluate -Dexpression=rundeck.job.option.value -q -DforceStdout', returnStdout: true
              if(rundeckOptionValue.startsWith('null object')) {
                rundeckOptionValue = null
              }
              rundeckOptions = getRundeckParams(rundeckJobParams, rundeckOptionName, rundeckOptionValue)
              print 'rundeck params for qa: ' + rundeckOptions
            }
            gfsRundeckJob(version: '',
                    param: rundeckOptions,
                    token: rundeckToken,
                    host: rundeckUrl,
                    job: rundeckJob,
                    maxRetries:params.maxRetries)
          }
        }
      }
      stage('QA regression test') {
        when {
          allOf {
            not {
              triggeredBy 'TimerTrigger'
            }
            expression {
              deployToQA && (branchName == "main" || branchName == "master" || branchName ==~ /release.*/)
            }
          }
        }
        steps {
          script {
            failedStage=env.STAGE_NAME
            waitTime=params.waitTimeForRegressions
            if(waitTime) {
              echo "Waiting ${waitTime} seconds for start up to complete prior starting regressions"
              sleep waitTime.toInteger()
            }
            try {
              sh "sh ./regression.sh QA"

            } catch(err) {
              withEnv(["JAVA_HOME=${tool jdkEnv}", "PATH=${tool jdkEnv}/bin:${env.PATH}"]) {
                rollbackJob = gfsPOMPropertyValue('rollback.rundeck.job', null, jdkEnv)
                if(rollbackJob) {
                    sh "echo rollback to previous version in qa"
                    sh """
    set +x
    mvn clean fs-rundeck:run-job -N -Drundeck.token=${rundeckToken} -Drundeck.job='\${rollback.rundeck.job}' -Drundeck.job.option.name='\${rollback.rundeck.job.option.name}' -Drundeck.job.option.value='\${rollback.rundeck.job.option.value}' -Drundeck.job.params='\${rollback.rundeck.job.params}'
  """
                }
              }
              error err.getMessage()
            }

            catchError(buildResult: 'SUCCESS', stageResult: 'UNSTABLE') {
                sh "echo trigger QA regression job"
                gfsRegressionJobs(auth: params.qaRegressionAuth, urls: params.qaRegressionJobs)
            }
          }
        }
      }
      stage('Dynamic Scan') {
        when {
          anyOf {
            triggeredBy 'TimerTrigger';
            expression {
              (branchName == "main" || branchName == "master" || branchName ==~ /hotfix.*/ || branchName ==~ /release.*/) && params.dynamicScanId && params.disableDynamicScan != true
            }
          }
        }
        steps {
          catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
            script {
              credId = 'zvc-svc-newarch-cicd-fortify-token'
              email = params.emailTo
              withCredentials([string(credentialsId: credId, variable: 'cloudCtrlToken')]) {
                gfsDynamicScan(dynamicScanId: params.dynamicScanId, cloudCtrlToken: cloudCtrlToken)
              }
            }
          }
        }
      }
    }
    post {
      failure {
        script {
          buildUrl = env.BUILD_URL
          buildUrl = buildUrl.replace("l1qvap1131.1dc.com:8443", "nsajenkins.1dc.com")
          mail to: params.emailTo, subject: "$JOB_NAME - Build # $BUILD_NUMBER failed at ${failedStage} stage", body: "Check console output at $buildUrl to view the results."
        }
      }
      always {
        script {
          currentBuild.result = currentBuild.result ?: 'SUCCESS'
          gfsNotifyGit(commitSha1: env.GIT_COMMIT)
          withEnv(["JAVA_HOME=${tool jdkEnv}", "PATH=${tool jdkEnv}/bin:${env.PATH}"]) {
            projectVersion = gfsPOMPropertyValue('project.version', null, jdkEnv)
          }
          sh "echo clean up workspace"
          if (params.buildContainerImage == true && image) {
              sh "docker rmi ${image.imageName}"
          }
          deleteDir()
        }
      }
    }
  }

  sh "echo project version = ${projectVersion}"
  return [releaseVersion: projectVersion]
}
