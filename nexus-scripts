

#!/bin/bash

# Nexus credentials and URL
NEXUS_USER="admin"
NEXUS_PASS="admin123"
NEXUS_URL="http://localhost:8081"

# Query repositories
curl -s -u $NEXUS_USER:$NEXUS_PASS -X GET "$NEXUS_URL/service/rest/v1/repositories" | jq '.[].name'




Certainly! Here's a shell script that helps you find the location of an artifact in Nexus Repository Manager:

```bash
#!/bin/bash

# Nexus configuration
NEXUS_URL="http://localhost:8081"
NEXUS_USER="admin"
NEXUS_PASS="admin123"

# Function to encode URL parameters
urlencode() {
    local string="${1}"
    local strlen=${#string}
    local encoded=""
    local pos c o

    for (( pos=0 ; pos<strlen ; pos++ )); do
        c=${string:$pos:1}
        case "$c" in
            [-_.~a-zA-Z0-9] ) o="${c}" ;;
            * )               printf -v o '%%%02x' "'$c"
        esac
        encoded+="${o}"
    done
    echo "${encoded}"
}

# Check if required parameters are provided
if [ "$#" -ne 3 ]; then
    echo "Usage: $0 <group_id> <artifact_id> <version>"
    exit 1
fi

GROUP_ID=$(urlencode "$1")
ARTIFACT_ID=$(urlencode "$2")
VERSION=$(urlencode "$3")

# Search for the artifact
SEARCH_URL="${NEXUS_URL}/service/rest/v1/search?repository=*&maven.groupId=${GROUP_ID}&maven.artifactId=${ARTIFACT_ID}&maven.baseVersion=${VERSION}"

RESPONSE=$(curl -s -u "${NEXUS_USER}:${NEXUS_PASS}" "${SEARCH_URL}")

# Check if the artifact was found
if [[ $(echo "${RESPONSE}" | jq '.items | length') -eq 0 ]]; then
    echo "Artifact not found."
    exit 1
fi

# Extract and display the artifact information
echo "${RESPONSE}" | jq -r '.items[] | "Repository: \(.repository)\nGroup: \(.group)\nName: \(.name)\nVersion: \(.version)\nAssets:"'
echo "${RESPONSE}" | jq -r '.items[].assets[] | "  - \(.path)\n    Download URL: \(.downloadUrl)"'
```

To use this script:

1. Save it to a file, e.g., `find_artifact.sh`
2. Make it executable: `chmod +x find_artifact.sh`
3. Run it with the required parameters: `./find_artifact.sh <group_id> <artifact_id> <version>`

For example:
```
./find_artifact.sh org.example myapp 1.0.0
```

This script does the following:

1. It sets up the Nexus configuration (URL and credentials).
2. It defines a function to URL-encode parameters.
3. It checks if the required parameters (group ID, artifact ID, and version) are provided.
4. It constructs a search URL using the provided parameters.
5. It sends a GET request to the Nexus API to search for the artifact.
6. If the artifact is found, it displays the repository, group, name, version, and asset information (including download URLs).

Make sure to replace the `NEXUS_URL`, `NEXUS_USER`, and `NEXUS_PASS` variables with your actual Nexus configuration.

This script requires `curl` for making HTTP requests and `jq` for parsing JSON responses. If these aren't installed on your system, you'll need to install them first.

Note: This script searches across all repositories. If you want to limit the search to specific repositories, you can modify the `SEARCH_URL` to include a specific repository name instead of the wildcard `*`.